#登录密码
tc_auth = "test:test123123"

#tc_ip = "192.168.0.110"
#tc_ip = "172.21.13.12"
tc_ip = "127.0.0.1"

redis_ip = "127.0.0.1"
#redis_ip = "192.168.0.110"
#redis_ip = "47.75.166.206"

redis_queue_key = 'wx_queue'

#请求路径
tc_path = "http://{}:8091/TotalControl/v2/".format(tc_ip)

#数据交互
#http_url = "http://admins.local.com"
#http_ip = "127.0.0.1"
http_ip = "192.168.0.110"
#http_url = "http://wxtestapi.huohu186.online"
http_url = "http://192.168.0.110"

#主控程序
main_app = "com.tencent.mm"

#现阶段配置型号
device_type = {
    # 华为青春版本
    'HUAWEI-HRY-AL00a' : 'huawei10',
    # 华为8C
    'HUAWEI-BKK-AL10' : 'huawei8c',
    # 红米Note8
    'Xiaomi-Redmi Note 8' : 'redminote8',
}