import redis
import json
import task
from configs.sysconfig import redis_ip, redis_queue_key

loop = 1
try :
    print('开始执行程序 v1')
    r = redis.StrictRedis(host=redis_ip, port=6379, db=0, decode_responses=True)
    while True:
        try:
            queue = r.brpop(redis_queue_key)
            print('第{}次循环'.format(loop))
            # print(type(queue[1]))
            data = json.loads(queue[1])
            print(data)
            # 初始化
            print('进入初始化')
            # 判断数据是否为JSON
            if data['task_name']:
                print('进入任务处理')
                task.task_queue(data)
            print('程序结束，等待下一次操作....')
            loop += 1
        except Exception as e:
            print('队列或解析异常{}' . format(e))
except Exception as e:
    print('Redis 连接异常 {}' . format(e))
    pass