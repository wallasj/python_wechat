import requests
import urllib
import urllib.parse
import urllib.request
#import pytesseract
import time
import json
import os
import ssl
import imghdr

#加载辅助文件
import helper

#加载配置文件
from configs.sysconfig import *
#from PIL import Image

ssl._create_default_https_context = ssl._create_unverified_context

class System:
    def __init__(self):
        print('系统初始化')
        self.base_path = tc_path
        #本地连接状态
        self.local_connection = False
        #服务器连接状态
        self.server_connection = False

        #if helper.check_network(tc_ip) :
        self.auth = tc_auth
        self.token = self.get_token(helper.base64str(self.auth))
        if not self.token :
            print('初始化token失败')
            return
        self.local_connection = True
        #self.server_connection = helper.check_network(http_ip)
        self.server_connection = True

    #获取token
    def get_token(self, encodeStr):
        print('获取连接token')
        try:
            url_login = self.base_path + 'login'
            headers = {'Authorization': encodeStr}
            resp = requests.get(url_login, headers=headers, timeout=10)
            return resp.json()['value']
        except requests.exceptions.RequestException as e:
            print(e)
        return []

    #获取设备ID
    def get_device(self):
        print('获取所有设备列表')
        url_device = self.base_path + 'devices'
        params = {
            'q': 'all',
            'token': self.token['token']
        }
        resp_dev = requests.get(url_device, params=params)
        return resp_dev.json()

    #获取设备属性
    def get_device_info(self, dev_id):
        print('获取设备信息{}' . format({dev_id}))
        url_device = self.base_path + 'devices/{}' . format(dev_id)
        params = {
            'token': self.token['token']
        }
        resp_dev = requests.get(url_device, params=params)
        r = resp_dev.json()
        if r['status'] :
            return resp_dev.json()
        return False

    #开启指定设备程序
    def run_device_app(self, dev_id, app_name):
        print('开始启动设备APP')
        restart_path = self.base_path + 'devices/{}/apps/{}'.format(dev_id, app_name)
        state = {"state": "active"}
        payload = dict(self.token, **state)
        payload = urllib.parse.urlencode(payload).encode('utf-8')
        requests.get(restart_path, params=payload)
        return True

    #关闭指定设备的程序
    def stop_device_app(self, dev_id, app_name):
        print('开始关闭设备APP')
        restart_path = self.base_path + 'devices/{}/apps/{}'.format(dev_id, app_name)
        state = {"state": "inactive"}
        payload = dict(self.token, **state)
        payload = urllib.parse.urlencode(payload).encode('utf-8')
        print('已停止应用')
        requests.get(restart_path, params=payload)
        return True

    #重启指定设备的程序
    def restart_device_app(self, dev_id, app_name):
        print('开始重启设备APP')
        restart_path = self.base_path + 'devices/{}/apps/{}'.format(dev_id, app_name)
        state = {"state": "restart"}
        payload = dict(self.token, **state)
        payload = urllib.parse.urlencode(payload).encode('utf-8')
        print('已停止应用')
        requests.get(restart_path, params=payload)
        return True

    #已安装的软件列表
    def app_list(self, dev_id):
        print('获取设备APP列表')
        url = self.base_path + 'devices/{}/apps?token={}&q=installed_apk_list'.format(dev_id, self.token.get('token'))
        resp = requests.get(url)
        return resp.json()

    #获取绝对路径
    def get_abs_filepath(self, filename):
        print('获取文件绝对路径')
        url = f"{self.base_path}storage?q=stat&file={filename}&token={self.token.get('token')}"
        resp = requests.get(url).json()
        return resp

    #从TC下载文件到本地
    def download_file(self, filename, savepath, name):
        print('开始下载本地文件')
        try :
            b = os.getcwd() + '/' + savepath
            if not os.path.exists(b):  # 判断当前路径是否存在，没有则创建new文件夹
                os.makedirs(b)
            url = f"{self.base_path}storage/{filename}"
            resp = requests.get(url)
            with open(b + name, "wb") as code:
                code.write(resp.content)
        except IOError as e:
            print("IOError")
        except Exception as e:
            print("Exception")
        return True

    #下载远程文件到本地
    def download_remote_file(self, url, savepath, name):
        print('开始下载远程文件')
        try :
            b = os.getcwd() + '/' + savepath
            if not os.path.exists(b):  # 判断当前路径是否存在，没有则创建new文件夹
                os.makedirs(b)
            file_suffix = os.path.splitext(url)[1]
            print(file_suffix)
            # 拼接图片名（包含路径）
            filename = '{}{}{}{}'.format(savepath, os.sep, name, file_suffix)
            # 下载图片，并保存到文件夹中
            result = urllib.request.urlretrieve(url, filename=filename)
            if result:
                #如果没有文件后缀
                if not file_suffix:
                    imgType = imghdr.what(filename)
                    if imgType:
                        print(imgType)
                        newName = (filename + '.' + imgType)
                        os.rename(filename, newName)
                        return newName
                return filename
        except IOError as e:
            print("IOError{}".format(e))
        except Exception as e:
            print("Exception{}".format(e))
        return None

    #输入文字
    def input_text(self, dev_id, text):
        print('开始输入文字')
        url = self.base_path + 'devices/' + dev_id + '/screen/texts'
        params = {
            'token': self.token.get('token'),
            'text': text
        }
        requests.post(url, params=params)

    #点击屏幕
    def tap_x_y(self, dev_id, pos, state='press'):
        print('开始点击坐标')
        x, y = pos[0], pos[1]
        url = self.base_path + 'devices/{}/screen/inputs'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'x': x,
            'y': y,
            'state': state
        }
        requests.post(url, params=params)

    #滑动屏幕
    def swipe(self, dev_id, direction, times=1):
        print('开始滑动屏幕')
        url = self.base_path + 'devices/{}/screen/inputs'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'direction': direction
        }
        for i in range(times):
            requests.post(url, params=params)
            time.sleep(0.5)

    #发送一个手机事件
    def send(self, dev_id, code, state='press'):
        print('开始执行手机操作{}'.format(code))
        url = self.base_path + 'devices/{}/screen/inputs'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'code': code,
            'state': state
        }
        resp = requests.post(url, params=params)
        return resp.json()

    #发送数据请求
    def send_url(self, url, data, method='get'):
        print('开始发起HTTP请求')
        params = {
            'data': json.dumps(data)
        }
        try :
            if method == 'post' :
                if type(data) == dict :
                    result = requests.post(http_url + url, params=data)
                else :
                    result = requests.post(http_url + url, params=params, headers={'Content-Type': 'application/json'})
            else :
                if type(data) == dict :
                    result = requests.get(http_url + url, params=data)
                else :
                    result = requests.get(http_url + url, params=params, headers={'Content-Type': 'application/json'})
            r = result.json()
            print('请求结果{}'.format(r))
            if r['code'] == 0:
                return r['data']
        except Exception:
            pass
        return False

    #发送带有附件的请求
    def send_file_url(self, url, filepath):
        print('开始发起带有附件的HTTP请求')
        files = {
            'file': open(filepath, 'rb')
        }
        try :
            result = requests.post(http_url + url, files=files)
            r = result.json()
            if r['code'] == 0:
                return r['data']
        except Exception:
            pass
        return False

    #内置的搜图方法，查找发送图标位置
    def seek_image(self, dev_id, img_file):
        print('开始查找图片')
        url = self.base_path + 'devices/{}/screen/images'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'file': img_file,
            'q': 'search',
            'sim': 0.8
        }
        resp = requests.get(url, params=params)
        return resp.json()

    #在指定区域内查找图片
    def seek_image_range(self, dev_id, img_file, rect):
        print('开始在范围内查找图片')
        url = self.base_path + 'devices/{}/screen/images'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'file': img_file,
            'rect': rect,
            'q': 'search',
            'sim': 0.8
        }
        resp = requests.get(url, params=params)
        return resp.json()

    #截取指定区域的图片
    def get_area_img(self, dev_id, type, rect):
        print('开始获取指定区域内截图')
        url = self.base_path + 'devices/{}/screen/images'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'type': type,
            'rect': rect
        }
        resp = requests.get(url, params=params)
        return resp.json()

    #识别区域内的文字
    def get_analyze_text(self, dev_id, rect, lang, mode):
        print('开始识别区域内文字')
        url = self.base_path + 'devices/{}/screen/texts'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'lang': lang,
            'rect': rect,
            'mode': mode,
            'sim' : 1
        }
        resp = requests.get(url, params=params)
        return resp.json()

    #获取区域内颜色的值
    def get_area_color(self, dev_id, rect):
        print('开始获取区域内颜色')
        url = self.base_path + 'devices/{}/screen/colors'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'q': 'color',
            'x': rect[0],
            'y': rect[1]
        }
        resp = requests.get(url, params=params)
        return resp.json()

    #滚动屏幕
    def scorll_screen(self, dev_id, dx, dy) :
        print('开始滚动屏幕')
        url = self.base_path + 'devices/{}/screen/inputs'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'x' : 0.5,
            'y' : 0.5,
            'scroll_dx': dx,
            'scroll_dy': dy
        }
        resp = requests.post(url, params=params)
        return resp.json()

    #获取手机剪切板内容
    def get_clipboard_text(self, dev_id):
        print('开始访问剪切板')
        url = self.base_path + 'devices/{}/screen/texts'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'type': 'clipboard_text'
        }
        resp = requests.get(url, params=params)
        return resp.json()

    #上传图片文件
    def upload_image_file(self, filepath, dir):
        print('开始上传文件')
        url = self.base_path + 'storage'
        newname = os.path.basename(filepath)
        files = {
            'images' : (
                dir + '/' + newname,
                open(filepath, 'rb'),
                'application/octet-stream'
            )
        }
        params = {
            'token': self.token.get('token')
        }
        resp = requests.post(url, files=files, params=params)
        return resp.json()

    #上传脚本文件
    def upload_script_file(self, filepath, dir):
        print('开始上传脚本')
        url = self.base_path + 'storage'
        newname = os.path.basename(filepath)
        files = {
            'scripts' : (
                dir + '/' + newname,
                open(filepath, 'rb'),
                'application/octet-stream'
            )
        }
        params = {
            'token': self.token.get('token')
        }
        resp = requests.post(url, files=files, params=params)
        return resp.json()

    #执行脚本
    def run_script(self, filename):
        print('开始执行上传脚本')
        url = self.base_path + 'script'
        params = {
            'token': self.token.get('token'),
            'file' : filename
        }
        resp = requests.post(url, params=params)
        return resp.json()

    #获取当前设备的屏幕截图
    def get_device_screen(self, dev_id, type):
        print('开始获取设备截图')
        url = self.base_path + 'devices/{}/screen/images'.format(dev_id)
        params = {
            'token': self.token.get('token'),
            'type': type
        }
        try:
            resp = requests.get(url, params=params)
            return resp.json()
        except Exception:
            print('请求失败')
        return None

    #截取区域内图片然后进行图片识别
    def area_words(self, dev_id, area):
        print('开始获取并识别区域内文字')
        cut = self.get_area_img(dev_id, 'png', area)
        filename = os.path.basename(cut['value'])
        self.download_file(cut['value'], 'cut_area/', filename)
        filepath = './cut_area/{}'.format(filename)
        if os.path.exists(filepath):
            print('开始识别图片文字')
            # 开始获取截图内容
            words = helper.basicGeneral(filepath)
            #image = Image.open(filepath)
            #words = pytesseract.image_to_string(image, lang='eng')
            print(words)
            # 删除文件
            #os.remove(filepath)
            return words
        return ''

    #下载本地文件到手机端并上传服务器
    def du_img(self, dpath):
        millis = int(round(time.time() * 1000))
        filename = '{}.jpg'.format(millis)
        filepath = './download/'
        self.download_file(dpath, filepath, filename)
        path = filepath + filename
        if os.path.exists(path):
            print('开始上传文件到服务器')
            # 上传到服务器
            files = {
                'file': open(path, 'rb')
            }
            result = requests.post(http_url + '/syn/upload_one_file', files=files)
            r = result.json()
            if r['code'] == 0:
                #helper.remove_file(path)
                return r['data']['path']
        return ''

    #下载远程文件到手机
    def dd_img(self, dev_id, url):
        millis = int(round(time.time() * 1000))
        filepath = self.download_remote_file(url, './remote', millis)
        print(filepath)
        if os.path.exists(filepath):
            # 上传图片到手机端
            upload = self.uploadFile(dev_id, filepath)
            #os.remove(filepath)
            if upload:
                return upload
        return ''

    # 上传文件
    def uploadFile(self, dev_id, localpath):
        # step 1.上传头像文件到服务器
        try:
            print('1.开始准备上传文件')
            path = 'script/' + dev_id
            result = self.upload_image_file(localpath, path)
            if (result['file']):
                # step 2.获取图片的绝对路径
                filename = os.path.basename(result['file'])
                nowpath = path + '/' + filename
                # print(nowpath)
                abs_path = self.get_abs_filepath(result['file'])
                print('2.获取文件绝对路径 ' + abs_path['value']['localFilename'])
                # print(abs_path)

                # step 3.创建本地JS文件
                print('3.创建JS文件')
                abs_path_new = abs_path['value']['localFilename'].replace('\\', '/')
                print('文件路径 ' + abs_path_new)
                script = "var device = Device.getMain(); var ret = device.upload('" + abs_path_new + "', '/sdcard/" + nowpath + "', 5000); if(ret == 0) { print('成功上传文件'); } else { print(lastError()); }"
                script_path = './scripts/' + dev_id + '/'
                helper.create_file(script_path, 'script.js', script)

                # # #step 4.移动本地文件到服务器
                print('4.移动脚本到服务器')
                self.upload_script_file(script_path + 'script.js', dev_id)
                #删除本地文件
                #os.remove(script_path + 'script.js')
                #time.sleep(5)

                # # #step 5.获取文件绝对路径
                new_script_path = 'scripts/' + dev_id + '/script.js'
                print('5.当前相对路径 ' + new_script_path)
                result = self.run_script(new_script_path)
                time.sleep(5)
                print(result)
                return result['status']
        except Exception as e:
            print('出现异常 {}'.format(e))
            return False
        return False