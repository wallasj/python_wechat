import funcs
import time
import random
import requests
import json

from configs.sysconfig import main_app, http_url

class Wx:
    def __init__(self, dev_id, mode=''):
        self.f = funcs.Funcs()
        self.id = dev_id
        self.f.init(self.id)
        self.wxinit = False
        self.max_time = 8
        self.temp_list = []

        if not self.f.start :
            print('初始化失败')
            return

        if not self.f.tc.server_connection :
            print('服务器连接异常')
            return

        #判断微信是否已安装
        apps = self.f.tc.app_list(self.id)
        if main_app not in apps['value']:
            print('未安装微信')
            #执行安装操作...未实现
            return

        #重启微信
        if mode != 'test' :
            self.f.tc.restart_device_app(self.id, main_app)
            time.sleep(2)
            loop = 0
            open = False
            while open == False:
                open = self.f.find_search_icon(self.id, self.f.c['imgs_btn']['wx_words'])
                if loop > self.max_time :
                    print('开启应用超时')
                    return
                loop += 1
                time.sleep(1)

        #开启成功
        self.wxinit = True
        print('微信开启成功')

    #获取个人资料
    def mine_info(self):
        print('-----开始获取用户信息-----')
        send_data = []
        self.f.tap_tools(self.id, ['mine', 'mine_step1'])

        #获取头像
        cut_avatar = self.f.tc.get_area_img(self.id, 'jpg', self.f.point_ec('mine_avatar_range'))
        avatar = self.f.tc.du_img(cut_avatar['value'])

        #获取微信号
        wx_account = self.f.tc.area_words(self.id, self.f.point_ec('wechat_mine_account'))

        #获取昵称
        nickname = ''
        if self.f.muti_search_icon(self.id, ['nickname']) :
            nickname = self.f.copy_input(self.id, 'nickname_text')
            print('昵称 - {}'.format(nickname))
            self.f.back(self.id, 2)

        #获取性别 和 签名
        sex = 1
        signatrue = ''
        if self.f.muti_search_icon(self.id, ['more']) :
            # 获取性别
            woman_tap = self.f.find_search_icon(self.id, self.f.c['imgs_btn']['woman'])
            print(woman_tap)
            if woman_tap :
                sex = 0

            #获取个性签名
            if self.f.muti_search_icon(self.id, ['signatrue']) :
                signatrue = self.f.copy_input(self.id, 'nickname_text')

        send_data.append({'device_id': self.id, 'device_name': self.f.di['name'], 'wx_account': wx_account, 'avatar': avatar,'nickname': nickname, 'signatrue' : signatrue, 'sex': sex})
        self.f.tc.send_url('/syn/device', send_data, 'post')
        print('-----获取当前用户信息结束-----')

    #修改个人资料
    def edit_profile(self, data):
        print('-----开始修改个人资料-----')
        if type(data) == dict :
            self.f.tap_tools(self.id, ['mine', 'mine_step1'])
            #修改头像
            if 'avatar' in data.keys() :
                print('--开始修改头像--')
                #上传头像
                if self.f.tc.dd_img(self.id, data['avatar']):
                    self.f.tap_tools(self.id, [
                        'mine_wx_avatar',
                        'choose_avatar_list',
                        'choose_avatar_first',
                        {'name' : 'choose_pictrues_first', 'time' : 2.5},
                        'choose_pictrues_btn'
                    ])
                    print('--修改头像成功--')

            #修改昵称
            if 'nickname' in data.keys() :
                print('--开始修改昵称--')
                if self.f.muti_search_icon(self.id, ['nickname']) :
                    loop = 0
                    self.f.tap_tools(self.id, ['nickname_input'])
                    while loop < 50:
                        self.f.tc.send(self.id, 'backspace')
                        loop += 1
                    time.sleep(1)

                    self.f.tc.input_text(self.id, data['nickname'])
                    time.sleep(1)
                    self.f.tap_tools(self.id, ['nickname_sure'])
                    print('--修改昵称成功--')

            if 'sex' in data.keys() or 'signatrue' in data.keys() :
                if self.f.muti_search_icon(self.id, ['more']) :
                    # 修改性别
                    if 'sex' in data.keys() :
                        self.f.tap_tools(self.id, ['choose_sex'])
                        print('--开始修改性别--')
                        if data['sex'] :
                            self.f.tap_tools(self.id, ['choose_male', 'sex_sure_btn'])
                        else :
                            self.f.tap_tools(self.id, ['choose_female', 'sex_sure_btn'])
                        print('--修改性别成功--')

                    #修改个性签名
                    if 'signatrue' in data.keys() :
                        print('--开始修改个性签名--')
                        if self.f.muti_search_icon(self.id, ['signatrue']) :
                            loop = 0
                            self.f.tap_tools(self.id, ['nickname_input'])
                            while loop < 50:
                                self.f.tc.send(self.id, 'backspace')
                                loop += 1
                            time.sleep(1)

                            self.f.tc.input_text(self.id, data['signatrue'])
                            time.sleep(1)
                            self.f.tap_tools(self.id, ['nickname_sure'])
                            print('--修改个性签名成功--')
            return True

        print('参数错误')

    #添加好友
    def add_friend(self, wechat_id):
        print('开始添加好友')
        if self.f.muti_search_icon(self.id, [
            'tianjia',
            'add_friend',
            'add_friend_searchbox'
        ]) :
            time.sleep(1.5)
            self.f.tc.input_text(self.id, wechat_id)
            time.sleep(2)
            self.f.tc.send(self.id, 'enter')
            if self.f.muti_search_icon(self.id, [
                'add_friend_result',
                'add_to_contact',
                'add_friend_send'
            ]):
                print('添加好友请求完成')

    #删除微信好友
    def del_friend(self, wechat_id):
        if not self._search_friend(wechat_id) :
            print('搜索好友失败')
            return
        if self.f.muti_search_icon(self.id, ['friend_icons']) :
            self.f.tap_tools(self.id, ['friend_avatar'])
            if self.f.muti_search_icon(self.id, ['friend_icons', 'del_friend']) :
                self.f.tap_tools(self.id, ['sure_delete_friend'])
                print('好友删除成功')
        return

    #获取微信好友列表
    def get_friends_list(self):
        print('-----开始获取微信好友列表-----')
        self.f.tap_tools(self.id, ['contact', 'contact_a'])
        loop = True
        move = False
        check_list = []
        friend_list = []
        start = self.f.point_ec('contact_begin')
        end_line = self.f.point_ec('contact_bottom_words_y')
        while loop:
            # 开始从第一个用户点击
            y = start[1]
            print(y)

            if y > end_line:
                print('停止点击')
                break

            # 判断是否到底部
            bottom = self.f.find_search_icon(self.id, self.f.c['imgs_btn']['lianxiren'])

            #print('------------------- start {}------------------'.format(start))
            color = self.f.tc.get_area_color(self.id, start)
            if color['value']['rgb'] in '0xffffff':
                # 点击进入详情
                if move :
                    self.f.tc.tap_x_y(self.id, start)
                else :
                    self.f.tc.tap_x_y(self.id, self.f.point_ec('contact_begin'))

                time.sleep(1)

                account = self._get_userinfo(check_list)
                if account != '':
                    check_list.append(account['wechat_account'])
                    friend_list.append(account)
                else:
                    self.f.back(self.id, 1)

            if bottom:
                if not move :
                    move = True
                    end_line = bottom['point'][1]
                    print('end_line : {}'.format(end_line))
                    y = self.f.point_ec('contact_last_screen_y')
                else :
                    #获取下一个色块
                    next_color = self.f.tc.get_area_color(self.id, [start[0], y + self.f.point_ec('contact_move_two')])
                    if next_color['value']['rgb'] in '0xffffff':
                        y += self.f.point_ec('contact_move_two')
                    else :
                        y += self.f.point_ec('contact_move_three')

                start = [start[0], y]
            else :
                if move :
                    #如果已经到底部，但是又回到滚动，说明异常
                    print('停止循环')
                    break

                self.f.tc.scorll_screen(self.id, 0, self.f.point_ec('contact_move_one'))
                time.sleep(1)

        print('-----微信好友获取结束-----')
        return friend_list

    #获取用户信息 - 内部方法
    def _get_userinfo(self, check_list):
        weixinhao = self.f.find_search_icon(self.id, self.f.c['imgs_btn']['weixinhao'])
        if weixinhao :
            nickname = ''
            remark = ''
            btn = [weixinhao['point'][0] + 80, weixinhao['point'][1]]
            self.f.tc.tap_x_y(self.id, btn, 'down')
            time.sleep(1)
            self.f.tc.tap_x_y(self.id, btn, 'up')
            time.sleep(1.5)
            self.f.muti_search_icon(self.id, ['copy'])
            time.sleep(2)

            #获取微信号
            patse = self.f.tc.get_clipboard_text(self.id)
            if not patse['status']:
                return ''

            wechat_account = patse['value']

            if wechat_account in check_list :
                return ''

            #获取头像
            cut_avatar = self.f.tc.get_area_img(self.id, 'jpg', self.f.point_ec('avatar_range'))
            avatar = self.f.tc.du_img(cut_avatar['value'])

            if self.f.muti_search_icon(self.id, ['friend_icons', 'set_remarks']) :
                #获取昵称
                nickname = self.f.copy_input(self.id, 'remark_input')

                #获取备注
                #判断是否已经设置了备注
                if self.f.muti_search_icon(self.id, ['new_remark_input']) :
                    remark = wechat_account
                    self.f.tc.input_text(self.id, remark)
                    time.sleep(1)
                    self.f.muti_search_icon(self.id, ['remark_finish'])
                #已经设置了备注
                else :
                    new_remarks = self.f.find_search_icon(self.id, self.f.c['imgs_btn']['new_remarks'])
                    print(new_remarks)
                    if new_remarks :
                        self.f.tc.tap_x_y(self.id, [new_remarks['point'][0] + 300, new_remarks['point'][1] + 100])
                        time.sleep(1)
                        self.f.tc.tap_x_y(self.id, [new_remarks['point'][0] + 300, new_remarks['point'][1] + 100], state='down')
                        time.sleep(1.5)
                        self.f.tc.tap_x_y(self.id, [new_remarks['point'][0] + 300, new_remarks['point'][1] + 100], state='up')
                        time.sleep(2)
                        if self.f.muti_search_icon(self.id, ['select_all', 'paste']):
                            copy = self.f.tc.get_clipboard_text(self.id)
                            print(copy)
                            if copy['value']:
                                remark = copy['value']
                                time.sleep(1.5)
                        self.f.back(self.id, 2)

                self.f.back(self.id, 1)

            self.f.back(self.id, 1)

            #print({'wechat_account': wechat_account, 'avatar': avatar, 'nickname': nickname, 'remark': remark})
            return {'wechat_account': wechat_account, 'avatar': avatar, 'nickname': nickname, 'remark': remark}
        return ''

    #同步用户信息
    def syn_friends(self):
        friend_list = self.get_friends_list()
        print(friend_list)
        time.sleep(2)
        send = []
        send.append({'device_id': self.id, 'friend_list': friend_list})
        time.sleep(1)
        params = {
            'data': json.dumps(send)
        }
        result = requests.post(http_url + '/syn/friends', params=params, headers={'Content-Type': 'application/json'})
        r = result.json()
        if r['code'] == 0 :
            print('同步微信好友成功')
        return None

    #搜索微信好友 - 内部方法
    def _search_friend(self, wechat_id):
        print('搜索好友')
        if self.f.muti_search_icon(self.id, ['sousuo']) :
            self.f.tc.input_text(self.id, wechat_id)
            time.sleep(2)
            self.f.tc.tap_x_y(self.id, self.f.point_ec('search_first_result'))
            time.sleep(2)
            return True
        return False

    #发送消息
    def send_msg(self, wechat_id, text, img=''):
        print('开始发送消息')
        if not self._search_friend(wechat_id) :
            print('搜索好友失败')
            return

        time.sleep(1)
        if img != '':
            #下载远程文件
            self.f.tap_tools(self.id, [
                'fujian_btn',
                'send_last_img',
                'send_last_img_list',
                'send_last_img_list_one',
                'send_last_img_list_select',
                'send_last_img_original',
                'send_last_img_btn'
            ])
        else :
            self.f.tc.input_text(self.id, text)
            time.sleep(2)
            self.f.muti_search_icon(self.id, ['send'])
        self.f.back(self.id, 2)

    #发送红包
    def send_lucky_money(self, wechat_id, money, password, num=0):
        print('开始发红包')
        if not self._search_friend(wechat_id) :
            print('搜索好友失败')
            return

        time.sleep(1)
        if self.f.muti_search_icon(self.id, ['fujian', 'red-package']):
            self.f.tap_tools(self.id, ['lucky_money_input'])
            time.sleep(1)
            self.f.tc.input_text(self.id, money)
            time.sleep(1)
            if num > 0 :
                self.f.tap_tools(self.id, ['lucky_money_num'])
                self.f.tc.input_text(self.id, num)
                time.sleep(1)
                self.f.tap_tools(self.id, ['lucky_money_blank'])
                time.sleep(3)
            else :
                time.sleep(3)
            if self.f.muti_search_icon(self.id, ['lucky_money_send']):
                #判断是否有风险文字
                warning = self.f.tc.area_words(self.id, self.f.point_ec('red-warning-info'))
                if '风险提醒' in warning :
                    self.f.tap_tools(self.id, ['warning-btn'])

                self.f.tc.input_text(self.id, password)
                time.sleep(3)
                self.f.back(self.id, 3)
                print('红包发送成功')

    #发送群红包
    def send_lucky_money_to_group(self):
        return

    #转账
    def tranfer_money(self, wechat_id, money, password):
        print('开始转账')
        if not self._search_friend(wechat_id) :
            print('搜索好友失败')
            return

        time.sleep(1)
        if self.f.muti_search_icon(self.id, ['fujian', 'transfer']):
            self.f.tap_tools(self.id, ['lucky_money_input'])
            self.f.tc.input_text(self.id, money)
            time.sleep(3)
            self.f.tap_tools(self.id, ['transfer_sure_btn'])
            time.sleep(5)

            #判断是否有风险文字
            warning = self.f.tc.area_words(self.id, self.f.point_ec('transfer-warning-info'))
            if '笔金额相同的' in warning :
                self.f.tap_tools(self.id, ['trasfer-warning-btn'])

            print('输入支付密码')
            self.f.tc.input_text(self.id, password)
            time.sleep(3)
            self.f.back(self.id, 3)
            print('转账成功')

    #发送朋友圈
    def send_moment(self, text, img=''):
        self.f.tap_tools(self.id, [
            'circle_friend',
            'circle-first'
        ])
        if img != '':
            time.sleep(3)
            #下载远程文件
            self.f.tap_tools(self.id, [
                'circle-text',
                'circle-from-photo',
                'choose_avatar_list',
                'choose_avatar_first',
                'circle-photo-first',
                'circle-photo-send-btn'
            ])

            if text :
                self.f.tap_tools(self.id, ['circle-text-input'])
                time.sleep(2)
                self.f.tc.input_text(self.id, text)
                time.sleep(1.5)

            self.f.tap_tools(self.id, ['circle-text-send-btn'])
        else :
            time.sleep(3)
            self.f.tc.tap_x_y(self.id, self.f.point_ec('circle-text'), 'down')
            time.sleep(2)
            self.f.tc.tap_x_y(self.id, self.f.point_ec('circle-text'), 'up')
            time.sleep(2)
            self.f.tap_tools(self.id, ['circle-text-input'])
            self.f.tc.input_text(self.id, text)
            time.sleep(1.5)
            self.f.tap_tools(self.id, ['circle-text-send-btn'])

    #随机点赞/评论
    def moment_handle(self, text=''):
        self.f.tap_tools(self.id, ['circle-step1'])
        if self.f.muti_search_icon(self.id, ['circle']):
            times = random.randint(1,5)
            print('随机滑动{}'.format(times))
            self.f.tc.swipe(self.id, 'up', times=times)
            time.sleep(3)
            if text:
                if self.f.muti_search_icon(self.id, ['circle-comment', 'circle-comment-btn']):
                    self.f.tc.input_text(self.id, text)
                    time.sleep(2)
                    if self.f.muti_search_icon(self.id, ['comment-sure']) :
                        print('评论完成')
            else :
                if self.f.muti_search_icon(self.id, ['circle-comment', 'circle-like-btn']):
                    print('点赞完成')

    #发送群数据同步群组
    def _send_syn_group_data(self, type):
        print('同步群数据为{}'.format(self.temp_list))
        if len(self.temp_list):
            send = []
            send.append({'device_id': self.id, 'group_list': self.temp_list})
            self.temp_list = []
            params = {
                'data': json.dumps(send),
                'type': type
            }
            result = requests.post(http_url + '/syn/groups', params=params, headers={'Content-Type': 'application/json'})
            r = result.json()
            if r['code'] == 0:
                print('同步群列表成功')
        return None

    #遍历群数据
    def _get_group_list(self, tapy = 0, screeny = 0):
        self.f.tap_tools(self.id, ['contact'])
        if self.f.muti_search_icon(self.id, ['groups']):
            if not self.f.muti_search_icon(self.id, ['no_group']) :
                if screeny > 0:
                    self.f.tc.scorll_screen(self.id, 0, screeny)
                    time.sleep(1)

                print('当前点击Y坐标{}'.format(tapy))
                start = self.f.point_ec('group_list_first')
                if tapy > 0:
                    start = [start[0], (start[1] + tapy)]

                # 判断是否已经到底部
                group_list = self.f.find_search_icon(self.id, self.f.c['imgs_btn']['group_list'])

                # 开始从第一个用户点击
                self.f.tc.tap_x_y(self.id, start)
                time.sleep(2)

                if self.f.muti_search_icon(self.id, ['group_icons']) :
                    print('是群，进入下一操作')

                    max_try = 10
                    loop = 1
                    # 循环查找坐标
                    while not self.f.find_search_icon(self.id, self.f.c['imgs_btn']['group_remark']):
                        self.f.tc.scorll_screen(self.id, 0, -210)
                        if loop > max_try :
                            print('异常停止')
                            return
                        loop += 1

                    #获取群名称
                    gname = ''
                    if self.f.muti_search_icon(self.id, ['group_name']):
                        #判断是否允许修改
                        words = self.f.tc.get_analyze_text(self.id, self.f.point_ec('group-info'), 'chi_sim','multiline')
                        print(words)
                        if '当前群' in words['value']:
                            print('没有权限')
                            self.f.back(self.id, 1)
                        else :
                            gname = self.f.copy_input(self.id, 'group_name_input')
                            self.f.back(self.id, 2)

                    #查看备注
                    remark = ''
                    if self.f.muti_search_icon(self.id, ['group_remark']):
                        remark = self.f.copy_input(self.id, 'group_new_remark_input')
                        self.f.back(self.id, 2)

                    if remark != '' or gname != '':
                        self.temp_list.append({'gname': gname, 'remark': remark})
                        self.f.back(self.id, 2)
                    else :
                        self.f.back(self.id, 3)

                    if group_list:
                        self._get_group_list(tapy + self.f.point_ec('group_move'), 0)
                    else:
                        self._get_group_list(0, self.f.point_ec('group_move'))
            else :
                print('没有任何群组')

    #获取列表
    def get_groups(self):
        self._get_group_list()
        self._send_syn_group_data('search')

    #创建群
    def create_group(self, friends, gname, remark):
        if self.f.muti_search_icon(self.id, ['tianjia', 'create_group']) :

            for wx in friends:
                self.f.tap_tools(self.id, ['create_group_input'])
                self.f.tc.input_text(self.id, wx)
                time.sleep(1)
                if self.f.find_search_icon(self.id, self.f.c['imgs_btn']['create_group_search_result']) :
                    self.f.tap_tools(self.id, ['create_group_first_result'])
                else :
                    self.f.tap_tools(self.id, ['create_group_first_result'])

            time.sleep(1.5)
            print('---确认建群---')
            self.f.tap_tools(self.id, [
                {'name' : 'create_group', 'time' : 3},
                {'name' : 'group_info', 'time' : 2}
            ])

            max_try = 10
            loop = 1
            # 循环查找坐标
            while not self.f.find_search_icon(self.id, self.f.c['imgs_btn']['group_remark']):
                self.f.tc.scorll_screen(self.id, 0, -210)
                if loop > max_try :
                    print('异常停止')
                    return
                loop += 1

            print('---开始设置群名称---')
            if self.f.muti_search_icon(self.id, ['group_name']) :
                self.f.tc.input_text(self.id, gname)
                time.sleep(2)
                self.f.muti_search_icon(self.id, ['group_remark_sure'])

                print('---开始写入群备注---')
                if self.f.muti_search_icon(self.id, ['group_remark']) :
                    self.f.tc.input_text(self.id, remark)
                    time.sleep(2)
                    if self.f.muti_search_icon(self.id, ['group_remark_sure']) :
                        print('---群备注设置成功---')

                        if self.f.find_tab_btn(self.id, 'save_group_to_contact') :
                            print('开始保存数据')
                            self.temp_list = []
                            self.temp_list.append({'gname': gname, 'remark': remark})
                            self._send_syn_group_data('create')

                            #保存本群用户
        return

    #获取群好友列表

    #随机订阅公众号
    def subcription(self, keywords):
        print('--开始订阅公众号--')
        self.f.tap_tools(self.id, ['circle-step1'])
        max_try = 10
        if self.f.muti_search_icon(self.id, ['souyisou']):
            loop = 0
            while not self.f.find_search_icon(self.id, self.f.c['imgs_btn']['souyisou_title']) :
                if loop > max_try:
                    print('进入订阅页面失败')
                    return
                loop += 1
                time.sleep(1)

            print('进入搜索页面')
            self.f.tap_tools(self.id, ['gongzhonghao'])

            print('开始检查是否到输入页面')
            loop = 0
            while self.f.find_search_icon(self.id, self.f.c['imgs_btn']['souyisou_title']) :
                self.f.tap_tools(self.id, ['gongzhonghao'])
                if loop > max_try:
                    print('进入订阅页面失败')
                    return
                loop += 1
                time.sleep(1)

            self.f.tc.input_text(self.id, keywords)
            time.sleep(3)
            self.f.tap_tools(self.id, ['souyisou-first'])
            time.sleep(3)
            self.f.tap_tools(self.id, ['souyisou-result-first'])
            time.sleep(3)
            self.f.muti_search_icon(self.id, ['subcription'])
            print('--订阅完成--')

    #阅读公众号文章
    def read_subcription(self):
        print('--开始准备随机阅读文章--')
        if self.f.muti_search_icon(self.id, ['sousuo']) :
            self.f.tap_tools(self.id, ['search_input'])
            self.f.tc.input_text(self.id, '订阅号消息')
            time.sleep(2)
            self.f.tap_tools(self.id, ['subscript_first_result'])

            #开始随机选择
            times = random.randint(1, 5)
            self.f.tc.swipe(self.id, 'up', times=times)
            time.sleep(1)
            self.f.tap_tools(self.id, ['subcriptions_choose'])

            #开始阅读
            times = random.randint(1, 5)
            for _ in range(times):
                self.f.tc.swipe(self.id, 'up')
                time.sleep(1)
            time.sleep(2)