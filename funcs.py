import system
import json
import time
import os

from configs.sysconfig import *
from configs.position import wxpos

class Funcs:
    def __init__(self):
        self.tc = system.System()
        #用来存储配置文件
        self.c = []
        #用来存储设备信息
        self.di = []
        #用来存储临时变量
        self.td = []
        #记录初始化
        self.start = False

    def find(self, name, path):
        """ 查找文件路径 """
        for root, dirs, files in os.walk(path):
            if name in files:
                return os.path.join(root, name)

    def init(self, dev_id):
        if not self.tc.local_connection :
            print('本地服务连接失败')
            return False

        #获取设备信息
        dinfo = self.tc.get_device_info(dev_id)
        print(dinfo)
        if not dinfo :
            print('func设备获取失败')
            return False

        self.di = dinfo['value']

        #判断设备类型，加载配置文件
        if self.di['name'] in device_type.keys() :
            file = device_type[self.di['name']]
        else:
            print('没有对应的配置文件')
            return False

        #加载配置文件
        config = self.find("{}.json" . format(file), os.path.abspath("."))
        with open(config, "r") as load_f:
        #with open("./configs/{}.json".format(file), 'r') as load_f:
            load_dict = json.load(load_f)

        self.c = load_dict

        self.start = True

    # 查找图片位置
    def find_search_icon(self, dev_id, path, rect=''):
        print('开始寻找 {}'.format(path))
        if not self.start:
            print('初始化失败')
            return False

        if rect :
            resp = self.tc.seek_image_range(dev_id, path, rect=rect)
        else :
            resp = self.tc.seek_image(dev_id, path)

        if resp.get('value'):
            v = resp['value']
            return {'point' : [v['x'], v['y']], 'rect' : v['rect']}

        return False

    #将多次点击结合
    def tap_tools(self, dev_id, taps):
        print('开始执行多次点击')
        if not self.start:
            print('初始化失败')
            return False

        for tap in taps :
            if type(tap) == dict :
                if 'name' in tap:
                    self.tc.tap_x_y(dev_id, self.point_ec(tap['name']))
                    if 'time' in tap :
                        time.sleep(tap['time'])
                    else :
                        time.sleep(1)
            else :
                self.tc.tap_x_y(dev_id, self.point_ec(tap))
                time.sleep(1)

    #将多次图片查找组合
    def muti_search_icon(self, dev_id, imgs, rect=''):
        print('开始多次查找')
        if not self.start:
            print('初始化失败')
            return False

        for img in imgs :
            if img in self.c['imgs_btn'].keys() :
                p = self.find_search_icon(dev_id, self.c['imgs_btn'][img], rect)
                if p :
                    self.tc.tap_x_y(dev_id, p['point'])
                    time.sleep(2.5)
                else :
                    print('未找到该图片{}'.format(img))
                    return False
            else :
                print('未设置图片')
                return False

        return True

    #长按复制与粘贴
    def copy_input(self, dev_id, name):
        print('执行长按与复制')
        if not self.start:
            print('初始化失败')
            return ''

        self.tc.tap_x_y(dev_id, self.point_ec(name))
        time.sleep(1)
        self.tc.tap_x_y(dev_id, self.point_ec(name), state='down')
        time.sleep(1.5)
        self.tc.tap_x_y(dev_id, self.point_ec(name), state='up')
        time.sleep(3)
        if self.muti_search_icon(dev_id, ['select_all', 'paste'], '') :
            copy = self.tc.get_clipboard_text(dev_id)
            if copy['value']:
                return copy['value']
        return ''

    #返回
    def back(self, dev_id, times):
        print('开始执行返回')
        if not self.start:
            print('初始化失败')
            return False

        i = 0
        while i < times :
            self.tc.send(dev_id, 'back')
            time.sleep(1)
            i += 1

    # 保存群按钮
    def find_tab_btn(self, dev_id, name):
        print('--开始寻找保存按钮--')
        save_btn = False
        max_try = 7
        times = 1
        while not save_btn:
            if times > max_try:
                return False

            save_btn = self.find_search_icon(dev_id, self.c['imgs_btn'][name])
            if not save_btn:
                self.tc.scorll_screen(dev_id, 0, -200)
                time.sleep(1)
            times += 1

        rect = [save_btn['rect'][2], save_btn['rect'][1] - 50, self.di['width'] - 20, save_btn['rect'][3] + 50]
        # self.f.tc.get_area_img(self.id, 'jpg', '{}'.format(rect))
        gb = self.find_search_icon(dev_id, self.c['imgs_btn']['group_save_btn'], '{}'.format(rect))
        print(gb)
        if gb:
            self.tc.tap_x_y(dev_id, gb['point'])
            time.sleep(1.5)
            print('开始点击保存')
            return True

        return False

    #坐标转换
    def point_ec(self, name):
        print('开始转换坐标{}'.format(name))
        if not self.start:
            print('初始化失败')
            return

        if int(self.di['width']) <= 0 and int(self.di['height']) <= 0:
            print('获取设备宽高异常')
            return

        if name not in wxpos.keys() :
            print('未找到当前坐标')
            return

        print('设备宽高{}, {}'. format(self.di['width'], self.di['height']))

        point = wxpos[name]
        #判断设备配置中是否有这个值，有则优先提取
        if len(self.c['fixed']) and name in self.c['fixed'].keys():
            print('进入特殊配置处理')
            point = self.c['fixed'][name]

        #print('转换的坐标{} 类型{}' . format(point, type(point)))

        if type(point) == dict:
            # 如果设置了X和y
            if 'x' in (point.keys()):
                return round(point['x'] * self.di['width'])
            if 'y' in (point.keys()):
                return round(point['y'] * self.di['height'])

        elif type(point) == list:
            return [
                    round(point[0] * self.di['width']),
                    round(point[1] * self.di['height'])
                   ]

        elif type(point) == str:
            p = json.loads(point)
            area = [
                round(p[0] * self.di['width']),
                round(p[1] * self.di['height']),
                round(p[2] * self.di['width']),
                round(p[3] * self.di['height'])
            ]
            print('四方坐标 {}' . format(area))
            return '{}'.format(area)
        else :
            return