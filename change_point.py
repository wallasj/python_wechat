import json
from configs.position import *

#坐标转换
def point_ec(name, dwidth, dheight, file=None):
    print('开始转换坐标{}'.format(name))

    if int(dwidth) <= 0 and int(dheight) <= 0:
        print('获取设备宽高异常')
        return

    if name not in wxpos.keys() :
        print('未找到当前坐标')
        return

    print('设备宽高{}, {}'. format(dwidth, dheight))

    c = []
    if file != None :
        with open("./configs/{}.json".format(file), 'r') as load_f:
            c = json.load(load_f)

    point = wxpos[name]
    #判断设备配置中是否有这个值，有则优先提取
    if len(c['fixed']) and name in c['fixed'].keys():
        print('进入特殊配置处理')
        point = c['fixed'][name]

    #print('转换的坐标{} 类型{}' . format(point, type(point)))

    if type(point) == dict:
        # 如果设置了X和y
        if 'x' in (point.keys()):
            return round(point['x'] * dwidth)
        if 'y' in (point.keys()):
            return round(point['y'] * dheight)

    elif type(point) == list:
        return [
                round(point[0] * dwidth),
                round(point[1] * dheight)
               ]

    elif type(point) == str:
        p = json.loads(point)
        area = [
            round(p[0] * dwidth),
            round(p[1] * dheight),
            round(p[2] * dwidth),
            round(p[3] * dheight)
        ]
        print('四方坐标 {}' . format(area))
        return '{}'.format(area)
    else :
        return

re = point_ec('circle-step1', 1000, 2000, 'huawei8c')
print(re)