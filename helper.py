import base64
import os
import re
import requests
import redis

from configs.sysconfig import redis_ip

#检查网络连接情况
def check_network(url):
    print('尝试连接 {} '.format(url))
    NETWORK_STATUS = False
    try:
        response = requests.get(url, timeout=5)
        if response.status_code == 200:
            NETWORK_STATUS = True
    except Exception:
        pass

    if not NETWORK_STATUS:
        print('连接服务器 {} 失败...'.format(url))

    return NETWORK_STATUS

#base64加密
def base64str(str_auth):
    encode_str = base64.b64encode(str_auth.encode("UTF-8"))
    encode_str = str(encode_str, 'utf8')
    return encode_str

#查找文件
def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)

#创建文件夹
def mkdir(path):
    folder = os.path.exists(path)
    if not folder:  # 判断是否存在文件夹如果不存在则创建为文件夹
        os.makedirs(path)  # makedirs 创建文件时如果路径不存在会创建这个路径
        print("文件夹创建成功")
    else:
        print("文件夹创建失败")

#保留小数位，但不四舍五入
def decimals(num):
    sp = str(num).split('.')
    if len(sp) < 4:
        return num
    try :
        new_num = re.findall(r"\d{1,}?\.\d{4}", str(num))
        if new_num :
            return float(new_num[0])
    except Exception:
        pass
    return False

#创建文件
def create_file(path, name, content):  # 定义函数名
    print('开始写入文件')
    b = os.getcwd() + '/' + path
    if not os.path.exists(b):  # 判断当前路径是否存在，没有则创建new文件夹
        os.makedirs(b)
    file = open(b + name, 'w')
    file.write(content)  # 写入内容信息
    file.close()
    return True

#删除本地文件
def remove_file(path):
    #判断文件是否存在
    if os.path.exists(path):
        os.remove(path)
    return None

__generalToken = 'https://aip.baidubce.com/oauth/2.0/token'
__generalBasicUrl = 'https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic'

#百度文字识别
def basicGeneral(img_path):
    # 二进制方式打开图片文件
    word = ''
    try :
        f = open(img_path, 'rb')
        img = base64.b64encode(f.read())
        params = {"image": img}
        access_token = get_baidu_token()
        request_url = __generalBasicUrl + "?access_token=" + access_token
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        response = requests.post(request_url, data=params, headers=headers)
        if response:
            result = response.json()
            if result['words_result']:
                word = result['words_result'][0]['words']
    except Exception as e:
        print('获取文字异常{}'.format(e))
        pass
    return word

def get_baidu_token() :
    token = ''
    try :
        pool = redis.ConnectionPool(host=redis_ip, port=6379, decode_responses=True)
        r = redis.Redis(connection_pool=pool)
        token = r.get('baidu_token')
        if not token :
            print('开始请求')
            host = __generalToken + '?grant_type=client_credentials&client_id=grvOBTsMkr6gO9A6WfgIyyGt&client_secret=up4TyYOu7WmRGPGVy5okU0GLY37dKoOO'
            response = requests.get(host)
            if response:
                result = response.json()
                # 设置redis
                r.set('baidu_token', result['access_token'], ex=result['expires_in'] - 3600)
                token = result['access_token']
        else:
            print('已存在，直接返回')
    except Exception:
        print('异常')
        pass
    return token