import system

class Device:
    def __init__(self):
        self.tc = system.System()
        self.init = False

        if not self.tc.local_connection :
            return

        if not self.tc.server_connection :
            return

        self.init = True

    #同步所有设备
    def syn_device_info(self):
        print('开始同步设备信息')
        send_data = []
        if self.init :
            devices = self.tc.get_device()
            for dev_id in devices['ids']:
                #获取设备信息
                dinfo = self.tc.get_device_info(dev_id)
                if not dinfo['value'] :
                    print('获取设备信息失败')
                    continue

                send_data.append({
                    'device_id': dev_id,
                    'device_name': dinfo['value']['name']
                })

                print(dev_id)

            self.tc.send_url('/syn/device', send_data, 'post')

    #修改设备APP状态
    def change_app_status(self, dev_id, app_name, status):
        print('开始修改设备软件状态')
        if type(status) == int and status in [0, 1]:
            if status == 0:
                self.tc.stop_device_app(dev_id, app_name)
            else :
                self.tc.run_device_app(dev_id, app_name)

            self.tc.send_url('/syn/update_profile', {
                'dev_no': dev_id,
                'type': 'is_online',
                'value': int(status)
            }, 'post')

    #获取设备截图
    def get_screen(self, dev_id):
        print('开始获取设备截图')
        path = self.tc.get_device_screen(dev_id, "jpg")
        if path:
            rpath = self.tc.du_img(path['value'])
            if rpath:
                # 发送请求给服务器，修改对应内容
                self.tc.send_url('/syn/update_profile', {
                    'dev_no': dev_id,
                    'type': 'screen_img',
                    'value': rpath
                }, 'post')