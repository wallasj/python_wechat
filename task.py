import device
import wx
import requests
import time

from configs.sysconfig import main_app, http_url

# 更新任务状态
def change_task(id, status, remark=''):
    print('进入状态修改')
    time.sleep(3)
    result = requests.post(http_url + '/syn/change_task', params={'id': id, 'status': status, 'remark' : remark})
    r = result.json()
    print(r)
    if r['code'] == 0:
        print('同步信息成功')

#任务分发
def task_queue(data) :
    try :
        #修改任务状态
        if data['task_id'] :
            change_task(data['task_id'], 1)

        # 批量上线
        if data['task_name'] == 'online':
            dev_id = data['dev_id']
            print('{}执行上线操作'.format(dev_id))
            d = device.Device()
            if d.init :
                d.change_app_status(dev_id, main_app, 1)
                change_task(data['task_id'], 2)
            else :
                change_task(data['task_id'], 3, '网络连接失败')

        # 批量下线
        elif data['task_name'] == 'offline':
            dev_id = data['dev_id']
            print('{}执行下线操作'.format(dev_id))
            d = device.Device()
            if d.init :
                d.change_app_status(dev_id, main_app, 0)
                change_task(data['task_id'], 2)
            else :
                change_task(data['task_id'], 3, '网络连接失败')

        # 同步所有设备信息
        elif data['task_name'] == 'syn_all_device':
            print('同步所有设备信息')
            d = device.Device()
            print(d.init)
            if d.init :
                d.syn_device_info()
                change_task(data['task_id'], 2)
            else :
                change_task(data['task_id'], 3, '网络连接失败')

        # 获取设备截图
        elif data['task_name'] == 'device_screen':
            print('获取设备截图')
            dev_id = data['dev_id']
            d = device.Device()
            if d.init :
                d.get_screen(dev_id)
                change_task(data['task_id'], 2)
            else :
                change_task(data['task_id'], 3, '网络连接失败')

        else :
            dev_id = data['dev_id']
            w = wx.Wx(dev_id)
            if w.wxinit:
                # 获取设备信息
                if data['task_name'] == 'get_device' :
                    # 发送消息
                    w.mine_info()
                    change_task(data['task_id'], 2)

                elif data['task_name'] == 'send_message':
                    # 判断内容是否有文本
                    # 判断用户总数
                    count_user = len(data['wx_accounts'])
                    print('总共发送人数{}'.format(count_user))
                    pic_loop = 0
                    if data['type'] == 'img':
                        if not w.f.tc.dd_img(dev_id, data['value']) :
                            change_task(data['task_id'], 3, '图片上传失败')
                            print('图片上传失败')
                            return

                    for user in data['wx_accounts']:
                        if data['type'] == 'text':
                            w.send_msg(user, data['value'])
                        elif data['type'] == 'img':
                            w.send_msg(user, '', data['value'])
                            pic_loop += 1
                        elif data['type'] == 'file':
                            print('暂不支持')
                        else:
                            print('未找到该消息类型')

                    change_task(data['task_id'], 2)

                # 添加微信好友
                elif data['task_name'] == 'add_friend':
                    w.add_friend(data['wechat_account'])
                    print('添加微信好友')
                    change_task(data['task_id'], 2)

                # 发送红包
                elif data['task_name'] == 'send_red_package':
                    print('发送红包')
                    count_user = len(data['wx_accounts'])
                    print('总共发送人数{}'.format(count_user))
                    for user in data['wx_accounts']:
                        if 'num' in data.keys() :
                            w.send_lucky_money(user, data['money'], data['pay_word'], data['num'])
                        else :
                            w.send_lucky_money(user, data['money'], data['pay_word'])
                    change_task(data['task_id'], 2)

                # 转账
                elif data['task_name'] == 'transfer_money':
                    print('转账')
                    count_user = len(data['wx_accounts'])
                    print('总共发送人数{}'.format(count_user))
                    for user in data['wx_accounts']:
                        w.tranfer_money(user, data['money'], data['pay_word'])
                    change_task(data['task_id'], 2)

                # 修改个人资料
                elif data['task_name'] == 'profile':
                    value = data['value']
                    if data['type'] == 'nickname':
                        print('修改昵称')
                        change_post = w.edit_profile({ 'nickname' : value })
                    elif data['type'] == 'avatar':
                        print('修改头像')
                        change_post = w.edit_profile({ 'avatar' : value })
                    elif data['type'] == 'sex':
                        print('修改性别')
                        change_post = w.edit_profile({ 'sex' : value })
                    elif data['type'] == 'signatrue':
                        print('修改个性签名')
                        change_post = w.edit_profile({ 'signatrue' : value })
                    elif data['type'] == 'pic':
                        print('暂不支持')
                        return
                    else:
                        print('未找到该类型')
                        return
                    print('资料修改完毕：{}'.format(change_post))
                    if change_post:
                        # 修改用户资料
                        w.f.tc.send_url('/syn/update_profile', {
                            'dev_no': dev_id,
                            'type': data['type'],
                            'value': value
                        }, 'post')
                        change_task(data['task_id'], 2)

                # 发送朋友圈
                elif data['task_name'] == 'send_moment':
                    if data['img']:
                        if not w.f.tc.dd_img(dev_id, data['img']):
                            print('图片上传失败')
                            return

                    w.send_moment(data['value'], data['img'])
                    print('发送朋友圈')
                    change_task(data['task_id'], 2)

                # 随机评论朋友圈
                elif data['task_name'] == 'moments_comment':
                    w.moment_handle(data['value'])
                    print('随机评论朋友圈')
                    change_task(data['task_id'], 2)

                # 随机点赞
                elif data['task_name'] == 'moments_like':
                    w.moment_handle()
                    print('随机点赞')
                    change_task(data['task_id'], 2)

                # 同步设备微信列表
                elif data['task_name'] == 'syn_wx_users':
                    w.syn_friends()
                    print('同步设备微信列表')
                    change_task(data['task_id'], 2)

                # 建立群组
                elif data['task_name'] == 'create_group':
                    w.create_group(data['friends'], data['group_name'], data['group_remark'])
                    print('建立群组')
                    change_task(data['task_id'], 2)

                # 同步微信群
                elif data['task_name'] == 'syn_group':
                    w.get_groups()
                    print('同步微信群')
                    change_task(data['task_id'], 2)

                # 随机阅读订阅号
                elif data['task_name'] == 'subscription':
                    print('随机阅读订阅号')
                    w.subcription(data['keywords'])
                    change_task(data['task_id'], 2)

                # 随机阅读公众号文章
                elif data['task_name'] == 'read_subscription':
                    w.read_subcription()
                    print('随机阅读公众号文章')
                    change_task(data['task_id'], 2)

                else:
                    print('未找到该命令')
                    change_task(data['task_id'], 3, '未找到该命令')
            else :
                change_task(data['task_id'], 3, '网络连接失败')
                #将设备状态改为下线
                print('设备获取异常')

    except BaseException as e:
        print('出现解析异常 {} '. format(e))